'use strict'

const http = require('http')
const socketio = require('socket.io')
const r = require('rethinkdb')
const config = require('./config').config

const server = http.createServer()
const io = socketio(server)
const port = process.env.PORT || 5151

r.connect(config.db, (err, conn) => {
  if (err) return console.error(err.message)

  r.table('images').changes().run(conn, (err, cursor) => {
    if (err) return console.error(err.message)

    cursor.on('data', data => {
      var image = data.new_val

      if (image.publicId != null && (data.old_val == null || data.old_val.publicId == undefined)) {
        io.sockets.emit('image', image)
      }
    })
  })
})

server.listen(port, () => console.log(`WS Listening on port ${port}`))

'use strict'

const config = {
  db: {
    host: '192.168.99.100',
    port: 28015,
    db: 'insta-clone-db'
  }
}

module.exports = { config }
